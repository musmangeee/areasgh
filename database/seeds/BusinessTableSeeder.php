<?php

use App\City;
use App\Business;
use Illuminate\Database\Seeder;

class BusinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('businesses')->truncate();
        $businesses = [
            [
                'name' => 'La Paradise Inn',
                'city_id' => '1',
                "user_id" => "1",


                'town_id' => '73',
                'phone' => '+233267081054',
                'url' => 'https://la-paradise-inn.1-hotels-gh.com/en/',
                'address' => 'Fourth Otswe St.',
                'landmark' => ''
            ],
            [
                'name' => "Kings Royal Atlantic",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "73",
                'phone' => '+233303960777',
                "url" => "https://kings-royal-atlantic-hotel.1-hotels-gh.com/en/",
                "address" => "675 3 4Th Ostwe Street",
                'landmark' => ''
            ],
            [
                'name' => "Deons Hotels",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "129",
                "phone" => "+233312298008",
                "url" => "",
                "address" => "No 1 Kaajaano Street",
                "landmark" => ""
            ],
            [
                'name' => "Embassy Gardens Premier Suites",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "33",
                "phone" => "+233556615573",
                "url" => "",
                "address" => "Fourth Circular Rd.",
                "landmark" => "St. Thomas Aquinas SHS"
            ],
            [
                'name' => "No. 1 Oxford St. Hotel & Suites",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233559889250",
                "url" => "",
                "address" => "1 Oxford St.",
                "landmark" => "Kona Grill"
            ],
            [
                'name' => "Van Der Salle Apartments ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233242255581",
                "url" => "",
                "address" => "Dadebu Rd.",
                "landmark" => "Lee Shoe Shop"
            ],
            [
                'name' => "The Ritzz Exlusive Guest House",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "100",
                "phone" => "+233264320371",
                "url" => "",
                "address" => "1 Dr. Esther Ocloo St.",
                "landmark" => "Regal Chinese"
            ],
            [
                'name' => "Del International Hospital",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233302543256",
                "url" => "",
                "address" => "65/67 Nii Sai Street",
                "landmark" => "PH Hotel"
            ],
            [
                'name' => "Ridge Hospital",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "100",
                "phone" => "+233247814770",
                "url" => "",
                "address" => "Castle Rd.",
                "landmark" => "Ghana Aids Commission"
            ],
            [
                'name' => "Kole Bu Teaching Hospital",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "69",
                "phone" => "+233302729510",
                "url" => "",
                "address" => "Guggisberg Ave.",
                "landmark" => ""
            ],
            [
                'name' => "Nyaho Medical Center",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "17",
                "phone" => "+233307086490",
                "url" => "",
                "address" => "35 Kofi Annan St.",
                "landmark" => ""
            ],
            [
                'name' => "Lister Hospital And Fertility Center",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "16",
                "phone" => "+233303409030",
                "url" => "https://www.listerhospital.com.gh/",
                "address" => "Airport Hills Blvd",
                "landmark" => ""
            ],
            [
                'name' => "C & J Hospital",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "130",
                "phone" => "+233303400036",
                "url" => "",
                "address" => "Adjaben Rd",
                "landmark" => ""
            ],
            [
                'name' => "37 Millitary Teaching Hospital",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "55",
                "phone" => "+233302777595",
                "url" => "",
                "address" => "Mills Rd.",
                "landmark" => "Negheli Barracks"
            ],
            [
                'name' => "Medimart Pharmacy",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "122",
                "phone" => "+233502296551",
                "url" => "",
                "address" => "Spintex Rd.",
                "landmark" => "Samsung Brand Store"
            ],
            [
                'name' => "Bedita Pharmacy",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "43",
                "phone" => "+233302778375",
                "url" => "",
                "address" => "Blohum Rd.",
                "landmark" => "King Of Kings School"
            ],
            [
                'name' => "Addpharma  Retail",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "98",
                "phone" => "+233302260712",
                "url" => "",
                "address" => "Reberrson Cres",
                "landmark" => "Qodesh"
            ],
            [
                'name' => "Equity Pharmacy",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233202532496",
                "url" => "",
                "address" => "Tanbu Street",
                "landmark" => ""
            ],
            [
                'name' => "Palace Pharmacy",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "33",
                "phone" => "+233241888616",
                "url" => "",
                "address" => "Ring rd E",
                "landmark" => ""
            ],
            [
                'name' => "Prime Pharmacy",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233501584305",
                "url" => "",
                "address" => "Garden Rd.",
                "landmark" => ""
            ],
            [
                'name' => "PillDoctor Pharmacy Gh.",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233267000104",
                "url" => "",
                "address" => "92 Boundary Rd.",
                "landmark" => "Citydia Supermarket"
            ],
            [
                'name' => "Fat Fish ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "127",
                "phone" => "+233551559949",
                "url" => "",
                "address" => "N Airport Rd.",
                "landmark" => "Villagio-Alto Towers"
            ],
            [
                'name' => "Coco Lounge",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "15",
                "phone" => "+233244222202",
                "url" => "",
                "address" => "N Liberation Link",
                "landmark" => "Stanbic Heights, Icon House"
            ],
            [
                'name' => "Azmera Restaurant",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "30",
                "phone" => "+233268029966",
                "url" => "azmerarestaurant.com",
                "address" => "9 Sir Arku KorsahRd.",
                "landmark" => "Expresso Head Office"
            ],
            [
                'name' => "Sunshine Salad Bar & Restaurant",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "33",
                "phone" => "+233244315703",
                "url" => "",
                "address" => "HP Nyemitei St",
                "landmark" => ""
            ],
            [
                'name' => "Buka Restaurant",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233244842464",
                "url" => "bukarestaurant.com",
                "address" => "10th St.",
                "landmark" => ""
            ],
            [
                'name' => "Urban Grill",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "15",
                "phone" => "+233246666000",
                "url" => "",
                "address" => "N Liberation Link",
                "landmark" => "Carbon Night Club"
            ],
            [
                'name' => "La Chaumiere",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "17",
                "phone" => "+233302772408",
                "url" => "",
                "address" => "131 Liberation Rd.",
                "landmark" => ""
            ],
            [
                'name' => "La Tandem",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "53",
                "phone" => "+233302762959",
                "url" => "",
                "address" => "6 Mankralo St.",
                "landmark" => ""
            ],
            [
                'name' => "Papa  Cuisine ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "26",
                "phone" => "+233501619320",
                "url" => "",
                "address" => "Casely Hayford Rd.",
                "landmark" => "Melcom"
            ],
            [
                'name' => "The Shop",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233302951958",
                "url" => "",
                "address" => "Omanye St",
                "landmark" => "Eyetsa"
            ],
            [
                'name' => "Global Mamas",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233244530467",
                "url" => "",
                "address" => "Ajoate  St.",
                "landmark" => "Koala"
            ],
            [
                'name' => "Moberry Agro",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "101",
                "phone" => "+233267391481",
                "url" => "",
                "address" => "Spintex Rd.",
                "landmark" => "Junction Mall"
            ],
            [
                'name' => "Vidya's Bookstore ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233302762704",
                "url" => "",
                "address" => "Oxford St.",
                "landmark" => ""
            ],
            [
                'name' => "Christie Brown",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233244418477",
                "url" => "",
                "address" => "809 Hp Nyemetei ST.",
                "landmark" => ""
            ],
            [
                'name' => "Penny Lane Real Estate Ghana Limited",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "17",
                "phone" => "+233244329343",
                "url" => "http://www.pennylaneproperty.com/",
                "address" => "Number 9 Sir Arku Korsah Road",
                "landmark" => "Roman Ridge Shopping Arcade"
            ],
            [
                'name' => "Ghana Prime Properties",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233201111151",
                "url" => "",
                "address" => "F/214 Basl RD.",
                "landmark" => ""
            ],
            [
                'name' => "SPHYNX (Property consultant)",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "44",
                "phone" => "+233302973871",
                "url" => "",
                "address" => "No. 1 Sebe St.",
                "landmark" => ""
            ],
            [
                'name' => "Westfields Real Estate",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "15",
                "phone" => "+233288970080",
                "url" => "",
                "address" => "3rd Floor, The Grand Oyeeman",
                "landmark" => ""
            ],
            [
                'name' => "CBC Properties",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "115",
                "phone" => "+233303935313",
                "url" => "",
                "address" => "3rd Floor - GM Plaza, La-Bawaleshie Road",
                "landmark" => "GM plaza"
            ],
            [
                'name' => "Regimanuel Gray Estates ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "122",
                "phone" => "+233501419091",
                "url" => "https://regimanuelgray.com/",
                "address" => "10 Asanfena Cresent",
                "landmark" => ""
            ],
            [
                'name' => "FS Real Estate Company",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "39",
                "phone" => "+233208190778",
                "url" => "",
                "address" => "Dansoman Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Protean Real Estate GH. LTD.",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "71",
                "phone" => "+233207007502",
                "url" => "",
                "address" => "Awoshie Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Emerald Properties",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "74",
                "phone" => "+233202223331",
                "url" => "http://www.emeraldpropertiesgh.com/",
                "address" => "8/8b 5th Circular Road",
                "landmark" => ""
            ],
            [
                'name' => "Real Living Home",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233248112219",
                "url" => "",
                "address" => "Nikoi Atsen Avenue",
                "landmark" => "Madina"
            ],
            [
                'name' => "Clifton Homes",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "17",
                "phone" => "+233204677033",
                "url" => "http://www.cliftonghana.com/",
                "address" => "34 Senchi St.",
                "landmark" => ""
            ],
            [
                'name' => "Goil Filling Station ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "2",
                "phone" => "",
                "url" => "",
                "address" => "134/24Abeka Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Tesano Total Service",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "123",
                "phone" => "+233302664925",
                "url" => "",
                "address" => "Abeka Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Shell Filling Station ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "85",
                "phone" => "+233302664636",
                "url" => "",
                "address" => "Kpakpo Mankralo St.",
                "landmark" => ""
            ],
            [
                'name' => "Engen Filling Station",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "11",
                "phone" => "",
                "url" => "",
                "address" => "Kojo Thompson Rd.",
                "landmark" => "Avenida Hotel"
            ],
            [
                'name' => "Asanska Jewellery Ltd.",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "63",
                "phone" => "+233302221523",
                "url" => "",
                "address" => "648/4 Royalt Rd.",
                "landmark" => "Asanska House"
            ],
            [
                'name' => "Emefa Jewellery Productions",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "51",
                "phone" => "+233277562188",
                "url" => "",
                "address" => "Haatso Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Morgan Touch Jewellery",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "104",
                "phone" => "+233234343434",
                "url" => "",
                "address" => "Oxford St.",
                "landmark" => "Sams Firma"
            ],
            [
                'name' => "Gold Pot Jewellery",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "124",
                "phone" => "+233244518867",
                "url" => "",
                "address" => "Oak St.",
                "landmark" => "Teshie 1ST Junction"
            ],
            [
                'name' => "Alisa Hotel",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "100",
                "phone" => "+233577555777",
                "url" => "",
                "address" => "19 Dr.  Isssert St.",
                "landmark" => ""
            ],
            [
                'name' => "Coconut Goove Regency ",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "114",
                "phone" => "+233302225155",
                "url" => "",
                "address" => "5 John Kasavubu Rd",
                "landmark" => "Ghana Immigration Service"
            ],
            [
                'name' => "Hotel Rema",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "31",
                "phone" => "+233243180689",
                "url" => "",
                "address" => "",
                "landmark" => ""
            ],
            [
                'name' => "Silicon Golf Apartment Hotel",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "130",
                "phone" => "",
                "url" => "",
                "address" => "8 Silicon Ave.",
                "landmark" => ""
            ],
            [
                'name' => "Kempinski Hotel",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "55",
                "phone" => "+233242436000",
                "url" => "",
                "address" => "66 Gamel Abdul Nasser Ave.",
                "landmark" => ""
            ],
            [
                'name' => "AH Hotel",
                "city_id" => "1", "user_id" => "1",

                "town_id" => "47",
                "phone" => "+233201775030",
                "url" => "",
                "address" => "84/86 1st Boundary Rd.",
                "landmark" => ""
            ],
            [
                'name' => "Silk Africana Limited",
                "city_id" => "1", "user_id" => "1",
                "town_id" => "50",
                "phone" => "+233550780764",
                "url" => "http://www.silkafricana.com/",
                "address" => "Cucumber Link",
                "landmark" => "A&C Mall"
            ],
            [
                "name" => "Vienna City",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "222",
                "phone" => "+233322023500",
                "url" => "",
                "address" => "No 4 Harper Road",
                "landmark" => "Golden Tulip"
            ],
            [
                "name" => "Oak Plaza",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "172",
                "phone" => "+233507168268",
                "url" => "",
                "address" => "Asokwa -Ahodwo roundabout road",
                "landmark" => "Kumasi City Mall"
            ],
            [
                "name" => "Golden Tulip Hotel-Kumasi City",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "222",
                "phone" => "+233322083777",
                "url" => "",
                "address" => "Rain tree street",
                "landmark" => ""
            ],
            [
                "name" => "West End Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "237",
                "phone" => "+233322022590",
                "url" => "",
                "address" => "Patase Main road",
                "landmark" => "Police Depot"
            ],
            [
                "name" => "Komfo Anokye Teaching Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "179",
                "phone" => "+233322022303",
                "url" => "",
                "address" => "Bantama to Kajetia road",
                "landmark" => ""
            ],
            [
                "name" => "Trust Care Specialist Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "244",
                "phone" => "+233208181056",
                "url" => "",
                "address" => "Dr K Addo Kuffour Avenue",
                "landmark" => "Friends Gardens restaurant"
            ],
            [
                "name" => "SDA Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "215",
                "phone" => "+233322032618",
                "url" => "",
                "address" => "Main Kumasi-Sunyani Road",
                "landmark" => ""
            ],
            [
                "name" => "Tasty Queen",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233245646581",
                "url" => "",
                "address" => "Star Junction",
                "landmark" => "Rexmar Hotel"
            ],
            [
                "name" => "Han Court Chinese Restaurant",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "198",
                "phone" => "+233201514698",
                "url" => "",
                "address" => "Major Kobinna Drive",
                "landmark" => "Miklin Hotel"
            ],
            [
                "name" => "African Pot Grill And Bar",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "242",
                "phone" => "+233268168200",
                "url" => "",
                "address" => "Patase Estate Road",
                "landmark" => ""
            ],
            [
                "name" => "The View Bar And Grill",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "156",
                "phone" => "+233244668880",
                "url" => "",
                "address" => "Melcom Road to Daban Road",
                "landmark" => "Enas Hybrid School"
            ],
            [
                "name" => "Kwik Save Pharmacy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "155",
                "phone" => "+233244288299",
                "url" => "",
                "address" => "KNUST Ahinsan Gate",
                "landmark" => "Allen Clinic"
            ],
            [
                "name" => "Menri Pharmacy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "148",
                "phone" => "+233322087902",
                "url" => "",
                "address" => "22nd Stewart Ave",
                "landmark" => ""
            ],
            [
                "name" => "Dannipharma Pharmacy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "177",
                "phone" => "",
                "url" => "",
                "address" => "Ohenenana K. Oppong Ave",
                "landmark" => "Ideal College"
            ],
            [
                "name" => "Costa Pharmacy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "210",
                "phone" => "+233244882484",
                "url" => "",
                "address" => "Komfo Anokye road",
                "landmark" => "Kumasi Zoo"
            ],
            [
                "name" => "Osons Chemist",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "177",
                "phone" => "+233244775805",
                "url" => "",
                "address" => "Osons Ave",
                "landmark" => "Adansi Bank"
            ],
            [
                "name" => "Nimo Pharmacy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "179",
                "phone" => "+233322025432",
                "url" => "",
                "address" => "Komfo Anokye roundabout to Bekwai Roundabout road",
                "landmark" => "Komfo  Anokye Hospital"
            ],
            [
                "name" => "Ikson Proprties Agency",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233322395160",
                "url" => "",
                "address" => "Bekwai Road",
                "landmark" => "Santasi Rondabout"
            ],
            [
                "name" => "Acheamfour Estates",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "237",
                "phone" => "+233322002542",
                "url" => "",
                "address" => "Patase Estate Road",
                "landmark" => "Top Martins"
            ],
            [
                "name" => "Properties Masters Ghana",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "172",
                "phone" => "+233244544487",
                "url" => "",
                "address" => "Millenium Plaza",
                "landmark" => "Top Martins Complex"
            ],
            [
                "name" => "Focus Properties",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "175",
                "phone" => "+233555075839",
                "url" => "",
                "address" => "Obei Nkwantabisa Ave",
                "landmark" => ""
            ],
            [
                "name" => "Fabares Consult",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "211",
                "phone" => "+233247744006",
                "url" => "",
                "address" => "New Manhyia Extention",
                "landmark" => "Santose"
            ],
            [
                "name" => "Mckeown Investment Co. Ltd",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "245",
                "phone" => "+233322099020",
                "url" => "",
                "address" => "27 Yaa Asantewaa Road",
                "landmark" => ""
            ],
            [
                "name" => "Frankies Gift Shop",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233242601078",
                "url" => "",
                "address" => "Obuasi Rd.",
                "landmark" => "Santase Rondabout"
            ],
            [
                "name" => "Palace Hypermart",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "156",
                "phone" => "+233242803906",
                "url" => "",
                "address" => "Dr Kuffuor Bypass",
                "landmark" => ""
            ],
            [
                "name" => "Electromart Asokwa Store",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "172",
                "phone" => "+233540101374",
                "url" => "",
                "address" => "Top Martins Complex",
                "landmark" => ""
            ],
            [
                "name" => "Unique Aromas",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "156",
                "phone" => "+233322034647",
                "url" => "",
                "address" => "Melcom Road to Daban Road",
                "landmark" => "Adiebeba Hospital"
            ],
            [
                "name" => "Franko Trading Enterprise",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "148",
                "phone" => "+233245316969",
                "url" => "",
                "address" => " Prempeh ll Street",
                "landmark" => "Agyaba Jeweleries"
            ],
            [
                "name" => "Freddies Corner",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "148",
                "phone" => "+233264356695",
                "url" => "",
                "address" => "22 Prempeh ll Street",
                "landmark" => "Stanbic Bank"
            ],
            [
                "name" => "Daksa Dental Clinic",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "156",
                "phone" => "+233261406060",
                "url" => "",
                "address" => "115 Melcom Road",
                "landmark" => "La Petite Pharmacy"
            ],
            [
                "name" => "Active Point Dental Clinic",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "194",
                "phone" => "+233244502648",
                "url" => "",
                "address" => " 174 Dr K Addo Kuffour Avenue",
                "landmark" => "Ultimate FM"
            ],
            [
                "name" => "Allied Filling Station",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233307002444",
                "url" => "",
                "address" => "Bekwai Road",
                "landmark" => "Rexmar Hotel"
            ],
            [
                "name" => "Benab Filling Station",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "142",
                "phone" => "+233244866466",
                "url" => "",
                "address" => "Abrepo Rd.",
                "landmark" => ""
            ],
            [
                "name" => "Shell Filling Station-Boadi Junction ",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "176",
                "phone" => "",
                "url" => "",
                "address" => "Jo. Mint Drive",
                "landmark" => "KNUST SHS"
            ],
            [
                "name" => "Total Filling Station-Kaase",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "203",
                "phone" => "+233322025301",
                "url" => "",
                "address" => "Prempeh I Street",
                "landmark" => "Kumasi Abattoir"
            ],
            [
                "name" => "Unity oil",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "160",
                "phone" => "+233541106218",
                "url" => "",
                "address" => "",
                "landmark" => ""
            ],
            [
                "name" => "Chez Royale Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "236",
                "phone" => "+233322048994",
                "url" => "",
                "address" => "102 Arcbishop Sarp. Rd.",
                "landmark" => "Aboude Fast Food"
            ],
            [
                "name" => "Homey Lodge",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "214",
                "phone" => "+233552515085",
                "url" => "",
                "address" => "9 Boakye Tenten Street",
                "landmark" => ""
            ],
            [
                "name" => "Royal Baron Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "157",
                "phone" => "+233557961002",
                "url" => "",
                "address" => "Theresa Asibey St",
                "landmark" => ""
            ],
            [
                "name" => "Wadoma Royale Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "220",
                "phone" => "+233322296089",
                "url" => "wadomayoyalehotel.com",
                "address" => "Sunyani  Rd.",
                "landmark" => ""
            ],
            [
                "name" => "Marbon Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233322081067",
                "url" => "",
                "address" => "Dr Kuffuor Bypass",
                "landmark" => "Santase Rondabout"
            ],
            [
                "name" => "Kwamo Executive Lodge",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "162",
                "phone" => "+233545212673",
                "url" => "",
                "address" => "Kwamo Fumesua Rd.",
                "landmark" => ""
            ],
            [
                "name" => "Okubi Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "142",
                "phone" => "+233322045554",
                "url" => "okubihotel.com",
                "address" => "Abrepo Rd.",
                "landmark" => "Kumasi Girls"
            ],
            [
                "name" => "Promised Land Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "229",
                "phone" => "+233266353581",
                "url" => "",
                "address" => "Crescent Avenue",
                "landmark" => ""
            ],
            [
                "name" => "Senator Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "163",
                "phone" => "+233558057497",
                "url" => "",
                "address" => "Kwadaso Road",
                "landmark" => ""
            ],
            [
                "name" => "Sunset Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "194",
                "phone" => "+233322036787",
                "url" => "",
                "address" => "20 Cedar Crescent",
                "landmark" => "Pass and Gardens"
            ],
            [
                "name" => "Nicolizy Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "198",
                "phone" => "+233322039068",
                "url" => "",
                "address" => "P.V. Obeng by Pass",
                "landmark" => "New Suame Salvation Primary"
            ],
            [
                "name" => "Agyaba Jewellery",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "148",
                "phone" => "+233277104488",
                "url" => "",
                "address" => "Prempeh ll St.",
                "landmark" => ""
            ],
            [
                "name" => "St Ben Jewellery",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "160",
                "phone" => "+233244284122",
                "url" => "",
                "address" => "Hudson Rd.",
                "landmark" => "Aboude Fast Food"
            ],
            [
                "name" => "City Of Gold Jewelleries",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "193",
                "phone" => "+233244806810",
                "url" => "cityofgoldjewellers.com",
                "address" => "Adiembra Rd",
                "landmark" => ""
            ],
            [
                "name" => "Goldland Jewellery",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "149",
                "phone" => "+233277883660",
                "url" => "",
                "address" => "Ohenenana K. Oppong Ave",
                "landmark" => "Total Filling Station"
            ],
            [
                "name" => "A Plus Jewellery",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "179",
                "phone" => "+233248334850",
                "url" => "aplusjewelrygh.business.site",
                "address" => "Cultural Centre",
                "landmark" => "Ikes Café"
            ],
            [
                "name" => "Sweet 16 Cosmetics",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "163",
                "phone" => "+233249129960",
                "url" => "",
                "address" => "Apire Road",
                "landmark" => "Deeper Life Bible Church"
            ],
            [
                "name" => "A1 Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "245",
                "phone" => "+233500940940",
                "url" => "",
                "address" => "Bekwai Rd.",
                "landmark" => ""
            ],
            [
                "name" => "Tafo Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "253",
                "phone" => "+233208121083",
                "url" => "",
                "address" => "",
                "landmark" => ""
            ],
            [
                "name" => "South Patasi Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "237",
                "phone" => "+233242481119",
                "url" => "",
                "address" => "Dr K Addo Kuffuor AVE.",
                "landmark" => ""
            ],
            [
                "name" => "Champion Divine Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "196",
                "phone" => "+233244230535",
                "url" => "",
                "address" => "Paul Adjei Duah Dr",
                "landmark" => "Royal Golf Club"
            ],
            [
                "name" => "Exotic Body and Reshape Consultancy",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "148",
                "phone" => "+233242572085",
                "url" => "",
                "address" => "Otumfo Osei Tutu II Blvd",
                "landmark" => "Beige Capital"
            ],
            [
                "name" => "North Suntreso Hospital",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "227",
                "phone" => "+233322022252",
                "url" => "",
                "address" => "Dr K Addo Kuffour Avenue",
                "landmark" => "Unity Oil"
            ],
            [
                "name" => "Rexmar Hotel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "241",
                "phone" => "+233553507471",
                "url" => "",
                "address" => "",
                "landmark" => "Allied Oil"
            ],
            [
                "name" => "Home Avenue Limited",
                "city_id" => "2", "user_id" => "1",

                "town_id" => "148",
                "phone" => "+233592941641",
                "url" => "",
                "address" => "",
                "landmark" => "Brotherman Spot"
            ],
            [
                "name" => "Home Avenue Limited",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "237",
                "phone" => "+233553507471",
                "url" => "",
                "address" => "",
                "landmark" => "Oyerepa FM"
            ],
            [
                "name" => "Truth City Chapel",
                "city_id" => "2", "user_id" => "1",
                "town_id" => "142",
                "phone" => "+233244804709",
                "url" => "",
                "address" => "",
                "landmark" => "Abrepo Junction"
            ],
            [
                "name" => "Eusbett Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "494",
                "phone" => "+233352024393",
                "url" => "null",
                "address" => "Sunyani-Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Nass Lodge",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "483",
                "phone" => "+233244867518",
                "url" => "null",
                "address" => "Plt 94 Berlin Top",
                "landmark" => "null"
            ],
            [
                "name" => "Moonlight Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "521",
                "phone" => "+233205741484",
                "url" => "null",
                "address" => "Sunyani Airport",
                "landmark" => "null"
            ],
            [
                "name" => "Thesyl Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "494",
                "phone" => "+233247523493",
                "url" => "null",
                "address" => "Cath. Univ. Avenue",
                "landmark" => "null"
            ],
            [
                "name" => "Chesville Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "487",
                "phone" => "+233352028412",
                "url" => "null",
                "address" => "Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Owusu Memorial Hospital",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "524",
                "phone" => "+233352023341",
                "url" => "null",
                "address" => "Atronie Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Green Hill Medical Center",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "482",
                "phone" => "+233208160764",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Constance Maternity Clinic",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233244739479",
                "url" => "null",
                "address" => "Cocoa Village",
                "landmark" => "null"
            ],
            [
                "name" => "St John Of God Hospital",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "495",
                "phone" => "+233352091547",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Joe Bennet Pharmacy",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "511",
                "phone" => "+233352027088",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Ballis Pharmacy",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "532",
                "phone" => "+233209073397",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Mossay Pharmacy",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "540",
                "phone" => "+233208235723",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Senti Chemist",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233352023745",
                "url" => "null",
                "address" => "Graphic Road",
                "landmark" => "null"
            ],
            [
                "name" => "Navipharma",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "527",
                "phone" => "+233208163870",
                "url" => "null",
                "address" => "kumasi-Sunyani Rd",
                "landmark" => "null"
            ],
            [
                "name" => "Muba Pharmacy",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "540",
                "phone" => "+233352023458",
                "url" => "null",
                "address" => "ZongoOne Way Street",
                "landmark" => "null"
            ],
            [
                "name" => "Joefiko Pharmacy Ltd",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "511",
                "phone" => "+233352027088",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "KFC",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233202439368",
                "url" => "null",
                "address" => "Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Mandy's Pizza",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "451",
                "phone" => "+233242750262",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Jakosa Gardens",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "481",
                "phone" => "+233507087959",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Tel Aviv Food Boutique",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "494",
                "phone" => "+233248848150",
                "url" => "null",
                "address" => "Sunyani-Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Mandela Restaurant",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "451",
                "phone" => "+233242750262",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Green Leaf Eatery",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233246429829",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Rushkus Ventures",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233200346618",
                "url" => "null",
                "address" => "Kumasi-Sunyani Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Electroland Gh. Ltd",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233232340207",
                "url" => "null",
                "address" => "Sunyani-Wenchi Rd",
                "landmark" => "null"
            ],
            [
                "name" => "Patmens Enterprise",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233540544018",
                "url" => "null",
                "address" => "Sunyani-Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Rokpoks Curtains",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233246113003",
                "url" => "null",
                "address" => "Sunyani-Wenchi Rd",
                "landmark" => "null"
            ],
            [
                "name" => "Franko trading Ent.",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233202765836",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Sam Bennet",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233352027247",
                "url" => "null",
                "address" => "Sunyani-Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Candiced Serviced Apartments",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "529",
                "phone" => "+233248979346",
                "url" => "null",
                "address" => "Sunyani- Abesim Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Ahodwo Houses And Properties",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "487",
                "phone" => "+23324332465",
                "url" => "null",
                "address" => "Sunyani Highway",
                "landmark" => "null"
            ],
            [
                "name" => "Tyco City Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233501333705",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Glamossay Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "451",
                "phone" => "+233544852858",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Sheila's Executive Hotel And Lodge",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "494",
                "phone" => "+233352195524",
                "url" => "null",
                "address" => "Sunyani-Berekum Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Batavia Hotel",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233546480006",
                "url" => "null",
                "address" => "Odumasi Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Tony.s Lodge ",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233267096152",
                "url" => "null",
                "address" => "Business School Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Hotel De Petra",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "null",
                "url" => "null",
                "address" => "New Rd.",
                "landmark" => "null"
            ],
            [
                "name" => "Tropical Hotel Ltd.",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233553209455",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Palmat Dental Clinic",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "523",
                "phone" => "+233569698477",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ],
            [
                "name" => "Oak Dental Clinic",
                "user_id" => "1", "city_id" => "6",
                "town_id" => "451",
                "phone" => "+233201660218",
                "url" => "null",
                "address" => "null",
                "landmark" => "null"
            ]
        ];

        foreach ($businesses as $business) {
            $business['slug'] = strtolower(str_replace(' ', '-', preg_replace("/[^ \w]+/", "", $business['name'])) . '-' . City::where('id', 1)->first()->name);
            $business['slug'] = strtolower(str_replace(' ', '-', preg_replace("/[^ \w]+/", "", $business['name'])) . '-' . City::where('id', 2)->first()->name);
            $business['slug'] = strtolower(str_replace(' ', '-', preg_replace("/[^ \w]+/", "", $business['name'])) . '-' . City::where('id', 6)->first()->name);
            Business::insert($business);
        }
    }
}
