<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','parent_id', 'icon', 'image'];

    public function category()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

}
