<?php

namespace App\Http\Controllers\Helper;

use App\Business;
use App\BusinessCategory;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Town;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class HelperController extends Controller
{

    public function array_flatten($array)
    {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function getClientIps()
    {
        $clientIps = array();
        $ip = $this->server->get('REMOTE_ADDR');
        if (!$this->isFromTrustedProxy()) {
            return array($ip);
        }
        if (self::$trustedHeaders[self::HEADER_FORWARDED] && $this->headers->has(self::$trustedHeaders[self::HEADER_FORWARDED])) {
            $forwardedHeader = $this->headers->get(self::$trustedHeaders[self::HEADER_FORWARDED]);
            preg_match_all('{(for)=("?\[?)([a-z0-9\.:_\-/]*)}', $forwardedHeader, $matches);
            $clientIps = $matches[3];
        } elseif (self::$trustedHeaders[self::HEADER_CLIENT_IP] && $this->headers->has(self::$trustedHeaders[self::HEADER_CLIENT_IP])) {
            $clientIps = array_map('trim', explode(',', $this->headers->get(self::$trustedHeaders[self::HEADER_CLIENT_IP])));
        }
        $clientIps[] = $ip; // Complete the IP chain with the IP the request actually came from
        $ip = $clientIps[0]; // Fallback to this when the client IP falls into the range of trusted proxies
        foreach ($clientIps as $key => $clientIp) {
            // Remove port (unfortunately, it does happen)
            if (preg_match('{((?:\d+\.){3}\d+)\:\d+}', $clientIp, $match)) {
                $clientIps[$key] = $clientIp = $match[1];
            }
            if (IpUtils::checkIp($clientIp, self::$trustedProxies)) {
                unset($clientIps[$key]);
            }
        }
        // Now the IP chain contains only untrusted proxies and the client IP
        return $clientIps ? array_reverse($clientIps) : array($ip);
    }

    public function set_category_preferences($search_category)
    {
        $categories = array_slice(Category::all('name')->toArray(), 0, 4);
        $categories = array_column($categories, 'name');

        $search_preferences = Cookie::get('searchPrefs');


        if ($search_preferences == null) {

            Cookie::queue(Cookie::make('searchPrefs', serialize($categories), 645000));

            // The user is visiting the first time
            return $categories;
        } else {
            $search_preferences = unserialize($search_preferences);
            $search_preferences_old = $search_preferences;
            if (($key = array_search($search_category, $search_preferences)) !== false) {
                unset($search_preferences[$key]);
            }
            if ($key != 0)
                [$search_preferences_old[$key - 1], $search_preferences_old[$key]] = [$search_preferences_old[$key], $search_preferences_old[$key - 1]];

            Cookie::queue(Cookie::make('searchPrefs', serialize($search_preferences_old), 645000));
        }

        return $search_preferences_old;
    }


    public function get_category_preferences()
    {
        $categories = array_slice(Category::all('name')->toArray(), 0, 4);
        $categories = array_column($categories, 'name');
        $search_preferences = Cookie::get('searchPrefs');
        if ($search_preferences == null) {
            Cookie::queue(Cookie::make('searchPrefs', serialize($categories), 645000));
            return $categories;
        } else {
            return unserialize($search_preferences);
        }
    }

    //    public function v($search_category)
    //    {
    //
    //
    //        $cities = array_slice(Town::all('name')->toArray(), 0, 4);
    //        $cities = array_column($cities, 'name');
    //        $search_preferences = Cookie::get('recentlocations');
    //
    //        if ($search_preferences == null) {
    //            Cookie::queue(Cookie::make('recentlocations', serialize($cities), 645000));
    //            // The user is visiting the first time
    //            return $search_category;
    //        } else {
    //            $search_preferences = unserialize($search_preferences);
    //            $search_preferences_old = $search_preferences;
    //            if (($key = array_search($search_category, $search_preferences)) !== false) {
    //                unset($search_preferences[$key]);
    //            }
    //            if ($key != 0)
    //                [$search_preferences_old[$key], $search_preferences_old[0]] = [$search_preferences_old[0], $search_preferences_old[$key]];
    //
    //            Cookie::queue(Cookie::make('recentlocations', serialize($search_preferences_old), 645000));
    //            return $search_preferences_old[0];
    //        }
    //
    //
    //
    //    }


    public function set_location_preferences($current_location)
    {


        $recent_locations = Cookie::get('recentlocations');
        if ($recent_locations == null) {
            $recent_locations = array(0 => 'A&C Mall, Accra');



            Cookie::queue(Cookie::make('recentlocations', serialize($recent_locations), 645000));
            // The user is visiting the first time
            $result = [
                'status' => true,
                'default' => true,
                'location' => $recent_locations,
                'message' => 'The cookie has been set!',
            ];
        } else {

            //            if (!is_array(unserialize($recent_locations)))
            //                dd($current_location, unserialize($recent_locations));
            $recent_locations = unserialize($recent_locations);

            array_unshift($recent_locations, $current_location);

            Cookie::queue(Cookie::make('recentlocations', serialize($recent_locations), 645000));
            $result = [
                'status' => true,
                'default' => false,
                'location' => $recent_locations,
                'message' => 'The cookie has been updated!',
            ];
        }

        return $result;
    }


    public function get_location_preferences()
    {
        $search_preferences = Cookie::get('recentlocations');
        if ($search_preferences !== null) {
            if (strpos(unserialize($search_preferences)[0], ',') == false) {
                $recent_locations = array(0 => 'A&C Mall, Accra');
                Cookie::queue(Cookie::make('recentlocations', serialize($recent_locations), 645000));
                return 'A&C Mall, Accra';
            }
        }



        return $search_preferences == null ? 'A&C Mall, Accra' : unserialize($search_preferences)[0];
    }


    public function get_prefer_wallpaper()
    {

        if (isset($this->get_category_preferences()[0])) {

            if ($this->get_category_preferences()[0] == 'Restaurants')
                $default_walpaper = 'restaurants.png';
            elseif ($this->get_category_preferences()[0] == 'Night Life')
                $default_walpaper = 'night-life.jpg';
            elseif ($this->get_category_preferences()[0] == 'Beauty & Spa')
                $default_walpaper = 'beauty.png';
            elseif ($this->get_category_preferences()[0] == 'Food')
                $default_walpaper = 'banner-new.jpg';
            else
                $default_walpaper = 'banner-new.jpg';
        } else {
            $default_walpaper = 'banner-new.jpg';
        }
        return $default_walpaper;
    }


    public function main_menu_data($searched_location = null, $search_category = '', $searched_keyword = '')
    {

        $pref_categories = $this->get_category_preferences();
        $pref_location = $city = $this->get_location_preferences();

        $pref_town = $town = Town::where('name', explode(',', trim($pref_location))[0])->first();


        $pref_city = City::where('id', $town->city_id)->first();
        $town_id = $town != null ? $town->id : 1;
        $top_area_businesses = Business::where('town_id', $town_id)->get();
        $new_area_businesses = Business::where('town_id', $town_id)->latest('created_at')->get()->take(3);
        return [
            'pref_location' => $pref_location,
            'pref_categories' => $pref_categories,
            'top_area_businesses' => $top_area_businesses,
            'new_area_businesses' => $new_area_businesses,
            'pref_town' => $pref_town,
            'pref_city' => $pref_city
        ];
    }


    public function parse_town($location)
    {
        return isset(explode(',', trim($location))[0]) ? trim(explode(',', trim($location))[0]) : '';
    }

    public function parse_city($location)
    {
        return isset(explode(',', trim($location))[1]) ? trim(explode(',', trim($location))[1]) : '';
    }

    /**
     * Validate Location Exist Or Not
     * @param $location
     * @return array
     */
    public function validate_location($location)
    {
        $parsed_town = $this->parse_town($location);
        $parsed_city = $this->parse_city($location);

        $town = Town::where('name', $parsed_town)->first();
        $city = City::where('name', $parsed_city)->first();
        if ($location == '') {
            $result['status'] = false;
            $result['message'] = 'The location is empty';
        } elseif ($town == null || $city == null) {
            $result['status'] = false;
            $result['message'] = 'The Town or City is non found in record!';
        } elseif ($city != null && $town != null) {
            $result['status'] = true;
            $result['message'] = 'The location is found in record!';
            $result['town'] = $town;
            $result['city'] = $city;
            $result['location_string'] = $result['town']['name'] . ' ' . $result['city']['name'];
        }
        return $result;
    }


    /**
     * Setting Cookie
     * @param $location
     * @return array
     */
    public function set_search_location($location)
    {

        $result = $this->validate_location($location);

        if ($result['status']) {
            $result['location_string'] = $result['town']['name'] . ', ' . $result['city']['name'];
            /*
             * Check If the location is changed?
             */
            if ($result['location_string'] != $this->get_location_preferences()) {
                // Location is changed
                $result['cookie_result'] = $this->set_location_preferences($location);
            } else {
                // Location is not changed
                $result['cookie_result'] = false;
            }
        }
        return $result;
    }


    /**
     * Returning Location
     * @param $location
     * @return array
     */
    public function get_location($location)
    {
        return $this->set_search_location($location);
    }


    public function get_find($find)
    {
        if ($find == null) {
            $result['status'] = true;
            $result['type'] = 'top_business_search';
        } elseif ($find != null) {
            $business = Business::where('name', $find)->first();
            $category = Category::where('name', $find)->first();
            if ($category != null) {
                $result['status'] = true;
                $result['type'] = 'category_search';
                $result['category'] = $category;
            } elseif ($business != null) {
                $result['status'] = true;
                $result['type'] = 'business_search';
                $result['business'] = $business;
            } else {
                $result['status'] = true;
                $result['type'] = 'keywords_search';
            }
        }
        $result['keywords'] = $find;
        return $result;
    }


    public function get_famous_town_businesses($location)
    {
        $location = $this->parse_town($location);
        $town_id = Town::where('name', $location)->first()->id;
        return  Business::where('town_id', $town_id)
            ->withCount('reviews')
            ->orderBy('reviews_count', 'desc')
            ->get();
    }
}
