<?php

namespace App\Http\Controllers\Frontend;

use App\City;
use App\Town;
use App\Review;
use App\Business;
use App\Category;
use App\Subscription;
use App\BusinessCategory;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Helper\HelperController;
use Illuminate\Http\Request;
class FrontEndController extends Controller
{


    public function index()
    {
        $helper = new HelperController();
        $pref_wallpaper = $helper->get_prefer_wallpaper();
        $data = $helper->main_menu_data();
        $data['pref_wallpaper'] = $pref_wallpaper;
        $data['cities'] = City::all();
        $data['random_categories'] = Category::where('parent_id', NULL)->get()->random(4);
        $data['recent_town_business_review'] = Review::whereIn('business_id', Business::where('town_id', $data['pref_town']->id)->get())->latest('created_at')->get()->unique('business_id')->take(9);
        $data['categories'] = Category::all();
        $data['categories_list'] = Category::where('parent_id', NULL)->get();

        return view('frontend.index', compact('data'));
    }


    public function all_cities()
    {

        $helper = new HelperController();
        $data = $helper->main_menu_data();
        $cities = City::all();

        return view('frontend.cities', compact('cities', 'data'));
    }

    public function subscription(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
        ]);

        $input = $request->all();

        $subscription = Subscription::create($input);
        return redirect()->back()->with('success','Email subscribe successfully');
    }

    public function faq()
    {

        $helper = new HelperController();
        $data = $helper->main_menu_data();
        

        return view('frontend.faq', compact('data'));
    }

    public function privacy_policy()
    {

        $helper = new HelperController();
        $data = $helper->main_menu_data();
        

        return view('frontend.privacy_policy', compact('data'));
    }

    public function terms_conditions()
    {

        $helper = new HelperController();
        $data = $helper->main_menu_data();
        

        return view('frontend.terms_conditions', compact('data'));
    }

}
