@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-12 col-lg-10 col-xl-8">

            <!-- Header -->
            <div class="header mt-md-5">
                <div class="header-body">
                    <div class="row align-items-center">
                        <div class="col">

                            <!-- Pretitle -->
                            <h6 class="header-pretitle">
                                 Town
                            </h6>

                            <!-- Title -->
                            <h1 class="header-title">
                               Update Town
                            </h1>

                        </div>
                    </div> <!-- / .row -->
                </div>
            </div>
            @if(Session::has('success'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
            @endif
            @if(Session::has('error'))
            <p class="alert {{ Session::get('alert-class', 'alert-danger') }}">{{ Session::get('error') }}</p>
            @endif

            <!-- Form -->
            <form role="form" action="{{route('town.update',$town->id)}}" method="POST" >
                <input type="hidden" name="_method" value="PUT">
                @csrf

                <div class="form-group">
                    <label for="city_id">Cities list:</label>
                 
                    <select class="form-control" id="city_id" name="city_id">
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name}}</option>
                        @endforeach
                    </select>
                </div>

                <!-- Project name -->
                <div class="form-group">

                    <!-- Label  -->
                    <label>
                        Town Name
                    </label>

                    <!-- Input -->
                    <input type="text" name="name" value="{{$town->name}}" class="form-control">

                </div>

                <!-- Divider -->
                <hr class="mt-5 mb-5">

                <!-- Buttons -->
                <input type="submit" name="submit" value="Update Town" class="btn btn-block btn-primary">

                {{-- <a href="#" class="btn btn-block btn-primary">
                    Create project
                </a> --}}
                <a href="{{route('town.index')}}" class="btn btn-block btn-link text-muted">
                    Cancel this Category
                </a>

            </form>

        </div>
    </div> <!-- / .row -->
</div>

@endsection

