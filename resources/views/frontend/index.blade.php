@extends('layouts.frontend')

@section('content')
    @include('frontend.partials.home_banner')

    <section>
        <div class="container text-center py-5 mt-5">
            <h2 class="h1">Find Best Businesses In Town</h2>


            <ul class="nav justify-content-center mt-3 border-bottom border-light">
                @for($a = 0; $a<4; $a++)
                    <li class="nav-item">
                        <a class="nav-link"
                           href="{{ url('search?location='.urlencode($data['cities'][$a]['name']).'&find=') }}">
                            {{ $data['cities'][$a]['name'] }}</a>
                    </li>
                @endfor
                <li class="nav-item">
                    <a class="nav-link"
                       href="{{ url('all_cities') }}">
                        <i class="fe fe-search pr-2"></i>More Cities</a>
                </li>
            </ul>

            <div class="row mt-5">

                @for($a = 0; $a<4; $a++)
                    <div class="col-lg-3 col-sm-6">
                        <a href="{{ url('search?find='.urlencode($data['random_categories'][$a]['name']).'&location=' . urlencode($data['pref_location'])) }}"
                           class="card cat-card lift">
                            <!-- Image -->
                            <img src="{{ asset('storage/'.$data['random_categories'][$a]['image']) }}" alt="..."
                                 class="card-img-top">
                            <!-- Body -->
                            <div class="card-body text-center py-3">
                                <!-- Heading -->
                                <h2 class="card-title text-dark">
                                    {{ $data['random_categories'][$a]['name'] }}
                                </h2>
                            </div>
                        </a>
                    </div>
                @endfor


            </div>
        </div>
    </section>

    <section class=" bg-white">
        <div class="container text-center py-5 mt-3">
            <h2 class="h1">{{ $data['pref_location'] }}</h2>

            <h3 class=" mt-5 mb-4">Hot & New Businesses</h3>
            <div class="row">
                @foreach($data['new_area_businesses'] as $business)
                    <div class="col-lg-4 col-sm-12">
                        <div class="card cat-card lift review-card">

                            @if(count($business->images) == 1)
                                <img src="{{ asset('storage/app/public/' . $business->images[0]->name) }}" alt="..."
                                     class="card-img-top">
                            @elseif(count($business->images) > 1)
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                                    <div class="carousel-inner">
                                        @foreach($business->images as $key => $image)
                                            <div class="carousel-item card-img-top @if(($key-1)==0) active @endif"
                                                 style="background-image: url('{{ asset('storage/app/public/' .$image->name) }}');">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                       data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                       data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            @else
                                <img src="{{ asset('backend/assets/img/avatars/profiles/avatar-2.jpg') }}" alt="..."
                                     class="card-img-top">
                            @endif
                            <!-- Image -->


                        <!-- Body -->
                            <div class="card-body text-left">
                                <h3 class="mb-1">
                                    <a href="{{ url('list-business', $business->slug)  }}">{{$business->name}}</a>
                                </h3>

                                <div class="row">
                                    <div class="col-auto">
                                        <a href="{{ url('write_a_review/'. $business->slug) }}">
                                            <div class="rating text-primary "
                                                 data-rate-value="@if(sizeof($business->reviews) > 1){{ floor((($business->reviews->sum('stars')/sizeof($business->reviews))*2)/2 ) }} @else 0 @endif"></div>
                                        </a>
                                    </div>
                                    <div class="col text-secondary total-reviews">
                                        {{ sizeof($business->reviews) }} Reviews Total
                                    </div>
                                </div>
                            @if ($business->category !="")
                                <p class="text-secondary">{{$business->category['name']}}</p>
                                @endif
                                <p class="text-primary"><i
                                            class="fe fe-home pr-2"></i>Opened {{ \Carbon\Carbon::parse($business->created_at)->diffForhumans() }}
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="container text-center py-5 mt-5">
        <h2 class="h1 mb-4">Recent Activity</h2>

        <div class="card-columns">
        @foreach($data['recent_town_business_review'] as $review)
            <!-- Card -->

                <div class="card review-card lift">
                    @if(count($review->images) == 1)
                        <img src="{{ asset('storage/app/public/' . $review->images[0]->name) }}" alt="..."
                             class="card-img-top">
                    @elseif(count($review->images) > 1)
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner">
                                @foreach($review->images as $key => $image)
                                    <div class="carousel-item card-img-top @if(($key-1)==0) active @endif"
                                         style="background-image: url('{{ asset('storage/app/public/' .$image->name) }}');">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                               data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                               data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                @endif
                <!-- Body -->
                    <div class="card-body">

                        <div class="mb-3">
                            <div class="row align-items-center">
                                <div class="col-auto">

                                    <!-- Avatar -->

                                    <a href="{{ url('user', $review->user['id']) }}" class="avatar">
                                        <img src="{{ asset('backend/assets/img/avatars/profiles/avatar-1.jpg') }}"
                                             alt="..." class="avatar-img rounded-circle">
                                    </a>

                                </div>
                                <div class="col ml-n2  text-left">

                                    <a href="{{ url('user', $review->user['id']) }}">
                                        <!-- Title -->
                                        <h4 class="mb-1">
                                            {{ $review->user['name'] }}
                                        </h4>
                                    </a>

                                    <!-- Time -->
                                    <p class="card-text small text-muted">
                                        <span class="fe fe-clock"></span>
                                        <time datetime="2018-05-24">{{ \Carbon\Carbon::parse($review->created_at)->diffForhumans() }}</time>
                                    </p>

                                </div>

                            </div> <!-- / .row -->
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <div class="rating text-primary " data-rate-value="{{ $review->stars }}"></div>

                                <p class="my-3 font-weight-bold font-italic font-size-lg">
                                    " {{ $review->text }}"
                                </p>
                            </div>
                        </div>
                    </div>

                    <!-- Footer -->
                    <div class="card-footer card-footer-boxed">
                        <a href="{{ url('business', $review->business['slug'])  }}">
                            <h4>{{ $review->business['name'] }}</h4>
                        </a>
                    </div>
                </div>


            @endforeach
        </div>
    </section>
    <section>
        <div class="container text-center py-5 mt-5">
            <h2 class="h1">Browse Business By Categories</h2>



            <div class="row mt-5">

                @for($a = 0; $a<7; $a++)
                    <div class="col-lg-3 col-sm-6">
                        <a href="{{ url('search?find='.urlencode($data['categories'][$a]['name']).'&location=' . urlencode($data['pref_location'])) }}"
                           class="card cat-card lift">
                            <!-- Image -->
                            <span class="{{ $data['categories'][$a]['icon'] }} py-5 text-primary" style="font-size: 3rem;"></span>
                            <!-- Body -->
                            <div class="card-body text-center py-3">
                                <!-- Heading -->
                                <h3 class="card-title text-dark">
                                    {{ $data['categories'][$a]['name'] }}
                                </h3>
                            </div>
                        </a>
                    </div>
                @endfor
                    <div class="col-lg-3 col-sm-6">
                        <a
                           data-toggle="collapse" href="#moreCategories" role="button" aria-expanded="false" aria-controls="moreCategories"
                           class="card cat-card lift">
                            <!-- Image -->
                            <span class="fa fa-ellipsis-h py-5 text-primary" style="font-size: 3rem;"></span>
                            <!-- Body -->
                            <div class="card-body text-center py-3">
                                <!-- Heading -->
                                <h3 class="card-title text-dark">
                                    More Categories
                                  </h3>
                            </div>
                        </a>
                    </div>

                    <div class="col-md-12 collapse" id="moreCategories">
                        <div class="card text-left">
                            <div class="card-body">
                                <div class="row">
                                    @foreach($data['categories_list'] as $category)
                                        <div class="col-md-12 mb-3"><h3><a href="{{ url('search?find='.urlencode($category['name']).'&location=' . urlencode($data['pref_location'])) }}">{{ $category->name }}</a></h3></div>
                                        @foreach($category->category as $category)
                                            <div class="col-md-3 mb-3"><a href="{{ url('search?find='.urlencode($category['name']).'&location=' . urlencode($data['pref_location'])) }}" class="text-secondary">{{ $category->name }}</a></div>
                                        @endforeach
                                        <br>
                                        <br>
                                        <br>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>
    @include('frontend.partials.footer')
@endsection
<!-- Libs JS -->

@section('script')

    <script>
        $(document).ready(function () {
            $(".rating").rate({
                update_input_field_name: $(".review_value"),
            });
        });
    </script>

@endsection
