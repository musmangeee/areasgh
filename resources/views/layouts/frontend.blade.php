<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>

@include('frontend.partials.head')

<body>
<div id="set_location" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Location</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
				<p>Please select your location.</p>
                <div class="row h-100">
            <form action="{{route('location')}}" method="GET" class="col-12">
                <div class="input-group mb-3 main-search">
                 
                    <input type="text" class="autocomplete_locations form-control" name="location" value="{{ $data['pref_location'] }}" placeholder="{{ $data['pref_location'] == '' ? 'Accra' : $data['pref_location'] }}" autocomplete="off">
                    <div class="input-group-prepend">
                        <button class="btn btn-primary rounded-right" type="submit"><i
                         class="fe fe-search mx-3"></i></button>
                    </div>
                </div>
            </form>
        </div>
            </div>
        </div>
    </div>
</div>


@include('frontend.partials.modals')
<div class="main-content">
    @yield('content')

</div>
@include('frontend.partials.scripts')


<script>
	$( document ).ready(function() {
  if (document.cookie.indexOf('visited=true') == -1){
    // load the overlay
    $('#set_location').modal({show:true});
    
    var year = 1000*60*60*24*365;
    var expires = new Date((new Date()).valueOf() + year);
    document.cookie = "visited=true;expires=" + expires.toUTCString();

  }set_locationset_location
}); 

</script>

@yield('script')
</body>

</html>
